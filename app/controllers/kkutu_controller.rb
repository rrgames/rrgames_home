class KkutuController < ApplicationController
  def index
  end
  def getRate (n)
    (n*n*n)*0.5 + (n*n)*1.4 + n*6.9 + 1
  end

  def getLev(rate)
    20.times do |i|
      if(getRate(i) > rate)
        return i;
      end
    end
  end

  def getImageByLev(lev)
    str = "/assets/image/level/lv00"
    if lev < 10
      str << "0"
    end
    str << lev.to_s
    str << ".png"
  end

  def getImage(rate)
    level = getLev(rate)
    str = "/assets/image/level/lv00"
    if level < 10
      str << "0"
    end
    str << level.to_s
    str << ".png"
  end

  def rank
    @arrT5 = []
    @arrKT = []
    @arrES = []
    @arrKK = []
    @arrJS = []
    @arrKS = []
    @score_top5 = User.all.order('score DESC').limit(5)
    @score_top5.each_with_index do |on, index|
      @arrT5[index] = getImage(on.score)
    end
    @ratingKT_top5 = User.all.order('rating_KT DESC').limit(5)
    @ratingKT_top5.each_with_index do |on, index|
      @arrKT[index] = getImage(on.score)
    end
    @ratingES_top5 = User.all.order('rating_ES DESC').limit(5)
    @ratingES_top5.each_with_index do |on, index|
      @arrES[index] = getImage(on.score)
    end
    @ratingKS_top5 = User.all.order('rating_KS DESC').limit(5)
    @ratingKS_top5.each_with_index do |on, index|
      @arrKS[index] = getImage(on.score)
    end
    @ratingKK_top5 = User.all.order('rating_KK DESC').limit(5)
    @ratingKK_top5.each_with_index do |on, index|
      @arrKK[index] = getImage(on.score)
    end
    @ratingJS_top5 = User.all.order('rating_JS DESC').limit(5)
    @ratingJS_top5.each_with_index do |on, index|
      @arrJS[index] = getImage(on.score)
    end
  end

  def system
    @arrLev = []
    @rate = []
    1.upto(20) do |i|
      @arrLev[i] = getImageByLev(i)
      @rate[i] = getRate(i)
    end
  end




end
