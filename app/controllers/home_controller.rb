class HomeController < ApplicationController
  def index
  end

  def contact
  end

  def policy
  end

  def privacy
  end
end
