# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 0) do

  create_table "global", id: false, force: true do |t|
    t.string  "name",  limit: 20
    t.integer "value"
  end

  add_index "global", ["name"], name: "name", unique: true, using: :btree

  create_table "user", id: false, force: true do |t|
    t.string    "id",            limit: 20
    t.string    "name",          limit: 16
    t.float     "score",         limit: 24,  default: 0.0,  null: false
    t.integer   "rating_KT",                 default: 1000, null: false
    t.integer   "rating_ES",                 default: 1000, null: false
    t.integer   "rating_KS",                 default: 1000, null: false
    t.integer   "rating_KK",                 default: 1000, null: false
    t.integer   "rating_JS",                 default: 1000, null: false
    t.integer   "normalPlay_KT",             default: 0,    null: false
    t.integer   "normalPlay_ES",             default: 0,    null: false
    t.integer   "normalPlay_KS",             default: 0,    null: false
    t.integer   "normalPlay_KK",             default: 0,    null: false
    t.integer   "normalPlay_JS",             default: 0,    null: false
    t.integer   "normalWin_KT",              default: 0,    null: false
    t.integer   "normalWin_ES",              default: 0,    null: false
    t.integer   "normalWin_KS",              default: 0,    null: false
    t.integer   "normalWin_KK",              default: 0,    null: false
    t.integer   "normalWin_JS",              default: 0,    null: false
    t.integer   "rankedPlay_KT",             default: 0,    null: false
    t.integer   "rankedPlay_ES",             default: 0,    null: false
    t.integer   "rankedPlay_KS",             default: 0,    null: false
    t.integer   "rankedPlay_KK",             default: 0,    null: false
    t.integer   "rankedPlay_JS",             default: 0,    null: false
    t.integer   "rankedWin_KT",              default: 0,    null: false
    t.integer   "rankedWin_ES",              default: 0,    null: false
    t.integer   "rankedWin_KS",              default: 0,    null: false
    t.integer   "rankedWin_KK",              default: 0,    null: false
    t.integer   "rankedWin_JS",              default: 0,    null: false
    t.float     "typeTime_KT",   limit: 24,  default: 0.0,  null: false
    t.float     "typeTime_ES",   limit: 24,  default: 0.0,  null: false
    t.float     "typeTime_KS",   limit: 24,  default: 0.0,  null: false
    t.float     "typeTime_KK",   limit: 24,  default: 0.0,  null: false
    t.float     "typeTime_JS",   limit: 24,  default: 0.0,  null: false
    t.float     "typeScore_KT",  limit: 24,  default: 0.0,  null: false
    t.float     "typeScore_ES",  limit: 24,  default: 0.0,  null: false
    t.float     "typeScore_KS",  limit: 24,  default: 0.0,  null: false
    t.float     "typeScore_KK",  limit: 24,  default: 0.0,  null: false
    t.float     "typeScore_JS",  limit: 24,  default: 0.0,  null: false
    t.integer   "ping",                      default: 0,    null: false
    t.datetime  "lastLoggedIn"
    t.string    "description",   limit: 200
    t.timestamp "joinedIn",                                 null: false
  end

  add_index "user", ["id"], name: "id", unique: true, using: :btree

  create_table "voca_ENG", id: false, force: true do |t|
    t.string  "word",          limit: 100
    t.string  "mean_KOR",      limit: 200
    t.integer "count_KT",                  default: 0, null: false
    t.integer "count_ES",                  default: 0, null: false
    t.integer "difficulty_KT",             default: 0, null: false
    t.integer "difficulty_ES",             default: 0, null: false
  end

  add_index "voca_eng", ["word"], name: "word", unique: true, using: :btree

  create_table "voca_JAP", id: false, force: true do |t|
    t.string  "word",          limit: 100
    t.string  "mean_KOR",      limit: 200
    t.integer "count_JS",                  default: 0, null: false
    t.integer "difficulty_JS",             default: 0, null: false
    t.string  "option",        limit: 10
  end

  add_index "voca_jap", ["word"], name: "word", unique: true, using: :btree

  create_table "voca_KOR", primary_key: "word", force: true do |t|
    t.string  "mean_KOR",      limit: 200
    t.integer "count_KS",                  default: 0,            null: false
    t.integer "count_KK",                  default: 0,            null: false
    t.integer "difficulty_KS",             default: 0,            null: false
    t.integer "difficulty_KK",             default: 0,            null: false
    t.string  "option",        limit: 10,  default: "0000000000", null: false
  end

  add_index "voca_kor", ["word"], name: "word", unique: true, using: :btree

end
